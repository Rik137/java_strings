package practice.regex;

import com.google.common.base.Joiner;

import java.util.Locale;
import java.util.StringJoiner;

public class SplitText {

    public static void main(String[] args) {

    }

    public static String splitTextIntoWords(String text) {
        StringBuilder builder = new StringBuilder();
        String regex = "[^a-zA-Z\\s]+";
        String newText = text.replaceAll(regex, "");
        String[] words = newText.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            builder.append(words[i]);
            if (i + 1 != words.length) {
                builder.append("\n");
            }
        }
        return builder.toString();
    }
}